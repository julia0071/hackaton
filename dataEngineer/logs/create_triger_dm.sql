CREATE or replace TRIGGER log_changes_response_status_project
AFTER INSERT OR UPDATE OR DELETE ON dm.response_status_project
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_response_status_task
AFTER INSERT OR UPDATE OR DELETE ON dm.response_status_task
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_response_user
AFTER INSERT OR UPDATE OR DELETE ON dm.response_user
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_response_project
AFTER INSERT OR UPDATE OR DELETE ON dm.response_project
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();
