CREATE or replace TRIGGER log_changes_user
AFTER INSERT OR UPDATE OR DELETE ON t_m.users
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_tasks
AFTER INSERT OR UPDATE OR DELETE ON t_m.tasks
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_statuses
AFTER INSERT OR UPDATE OR DELETE ON t_m.statuses
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_projects
AFTER INSERT OR UPDATE OR DELETE ON t_m.projects
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_project_responsible
AFTER INSERT OR UPDATE OR DELETE ON t_m.project_responsible
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_locations
AFTER INSERT OR UPDATE OR DELETE ON t_m.locations
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or REPLACE TRIGGER log_changes_job_titles
AFTER INSERT OR UPDATE OR DELETE ON t_m.job_titles
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_departments
AFTER INSERT OR UPDATE OR DELETE ON t_m.departments
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();

CREATE or replace TRIGGER log_changes_comments
AFTER INSERT OR UPDATE OR DELETE ON t_m.comments
FOR EACH ROW
EXECUTE FUNCTION logs.log_changes();