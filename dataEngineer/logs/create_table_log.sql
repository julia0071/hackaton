create table if not exists logs.changes_log (
    change_id serial PRIMARY KEY,
    table_name text NOT NULL,
    operation text NOT NULL,
    old_data jsonb,
    new_data jsonb,
    change_date timestamp DEFAULT current_timestamp
);