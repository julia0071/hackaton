
<<<<<<< HEAD:dataEngineer/create_db/data_base_corr.sql
/*drop schema if exists t_m cascade;*/
=======
drop schema if exists t_m cascade;
>>>>>>> 9a45e08f691f6c2ee0386ebcb2925c447270ef83:dataEngineer/data_base_corr.sql

/*enum создаты на беке для упрощения работы по синхронизации в короткие сроки, если бы было время то создавали бы таблицы с внешними ключами*/
create schema if not exists t_m;

create table if not exists t_m.job_titles (
	id_job_title serial,
	name_job_title varchar(30) NOT NULL,
	constraint job_id primary key(id_job_title),
	constraint j_name unique (name_job_title),
	constraint upper_letter_name_job_title check (left(name_job_title,1) = left(initcap(name_job_title),1))
);

create index if not exists job_titles_in
on t_m.job_titles (name_job_title);

create table if not exists t_m.locations (
	location_id serial,
	country varchar(30) NOT NULL,
	city varchar(30) NOT NULL,
	constraint loc_id primary key(location_id),
	constraint loc_coun unique (country, city),
	constraint upper_letter_country check (left(country,1) = left(initcap(country),1)),
	constraint upper_letter_city check (left(city,1) = left(initcap(city),1))
);

create table if not exists t_m.departments (
	department_id serial,
	name_department varchar(100) NOT NULL,
	constraint dep_id primary key(department_id),
	constraint dep_name unique (name_department),
	constraint upper_letter_name_department check (left(name_department,1) = left(initcap(name_department),1))
);

create table if not exists t_m.statuses (
	status_id serial,
	status_name varchar(30) NOT NULL,
	constraint pr_status primary key(status_id),
	constraint pr_st_na unique (status_name),
	constraint upper_letter_status_name check (left(status_name,1) = left(initcap(status_name),1))
);

create index if not exists statuses_in
on t_m.statuses (status_name);

create table if not exists t_m.users (
	user_id serial,
	id_job_title int,
	first_name varchar(30) NOT NULL,
	last_name varchar(30) NOT NULL,
	second_name varchar(30),
	email text NOT NULL,
	password varchar NOT NULL,
	department_id int,
	telephone varchar(20),
	created_date timestamp DEFAULT current_timestamp,
	location_id int,
	refresh_token varchar (45),
	expiration_date_token timestamp,
	verify_code int,
	verify_status boolean NOT NULL,
	name_role varchar(40) NOT NULL,
	constraint us_id primary key(user_id),
	constraint phon unique (telephone),
	constraint email_con unique (email),
	constraint us_type FOREIGN KEY (id_job_title) REFERENCES t_m.job_titles(id_job_title) ON DELETE set null,
	constraint dep_id FOREIGN KEY (department_id) REFERENCES t_m.departments(department_id) ON DELETE set null,
	constraint loc_id FOREIGN KEY (location_id) REFERENCES t_m.locations(location_id) ON DELETE set null,
	constraint upper_letter_first_name check (first_name = initcap(first_name)),
	constraint upper_letter_last_name check (last_name = initcap(last_name))
);

create index if not exists idx_first_last_name
on t_m.users (last_name, first_name);

create index if not exists idx_id_user_type
on t_m.users (id_job_title);

create index if not exists  idx_department_id
on t_m.users (department_id);

create index if not exists idx_location_id
on t_m.users (location_id);

create table if not exists t_m.projects (
	project_id bigserial,
	project_title varchar(200) NOT NULL,
	goal varchar(200) NOT NULL,
	start_plan_date timestamp NOT NULL,
	start_on timestamp,
	end_plan_date timestamp NOT NULL,
	closed_on timestamp,
	estimate_time_plan varchar(10) NOT NULL,
	estimate_time_fact varchar(10),
	project_description varchar NOT NULL,
	status_id int,
	owner_id int not null,
	project_type_name varchar(40) NOT NULL,
	created_on timestamp DEFAULT current_timestamp,
	constraint proj_id primary key(project_id),
	constraint ptoj_tit unique (project_title),
	constraint pro_st FOREIGN KEY (status_id) REFERENCES t_m.statuses(status_id) ON DELETE set null,
	constraint own_con FOREIGN KEY (owner_id) REFERENCES t_m.users(user_id) ON DELETE restrict,
	constraint end_date_plan_pr check(end_plan_date>start_plan_date),
	constraint end_date_fact_pr check(closed_on>start_on),
	constraint check_estimate_time_format_pr CHECK (estimate_time_plan ~ '^\d+[dwm]$'),
	constraint check_estimate_time_fact_pr CHECK (estimate_time_fact ~ '^\d+[dwm]$')
);

create index if not exists idx_project_title
on t_m.projects (project_title);

create index if not exists ix_pro_st
on t_m.projects (status_id);

create index if not exists ix_own
on t_m.projects (owner_id);

create table if not exists t_m.tasks (
	issue_id bigserial,
	subject varchar(300) NOT NULL,
	description varchar NOT NULL,
	task_type_name varchar(30) not null,
	start_plan_date timestamp,
	start_on timestamp,
	end_plan_date timestamp,
	closed_on timestamp,
	estimate_time_plan varchar(10),
	estimate_time_fact varchar(10),
	owner_id int,
	assigned_to_id int,
	parent_id int,
	project_id int,
	priority_name varchar(40),
	status_id int,
	created_on timestamp DEFAULT current_timestamp,
	constraint ts_id primary key(issue_id),
	constraint tk_project unique (subject, project_id),
	constraint end_date_plan check(end_plan_date>=start_plan_date),
	constraint end_date_fact check(closed_on>=start_on),
	constraint own_us FOREIGN KEY (owner_id) REFERENCES t_m.users(user_id) ON DELETE restrict,
	constraint assign_us FOREIGN KEY (assigned_to_id) REFERENCES t_m.users(user_id) ON DELETE restrict,
	constraint pr_id FOREIGN KEY (project_id) REFERENCES t_m.projects(project_id) ON DELETE restrict,
	constraint st_id FOREIGN KEY (status_id) REFERENCES t_m.statuses(status_id) ON DELETE set null,
	constraint check_estimate_time_plan_t CHECK (estimate_time_plan ~ '^\d+[hdwm]$'),
	constraint check_estimate_time_fact_t CHECK (estimate_time_fact ~ '^\d+[hdwm]$')
);

ALTER TABLE t_m.tasks
ALTER COLUMN project_id SET NOT NULL;

ALTER TABLE t_m.tasks
ALTER COLUMN task_type_name SET NOT NULL;

ALTER TABLE t_m.tasks
ALTER COLUMN start_plan_date SET NOT NULL;

ALTER TABLE t_m.tasks
ALTER COLUMN end_plan_date SET NOT NULL;

ALTER TABLE t_m.tasks
ALTER COLUMN estimate_time_plan SET NOT NULL;

ALTER TABLE t_m.tasks
ALTER COLUMN owner_id SET NOT NULL;

ALTER TABLE t_m.tasks
ALTER COLUMN priority_name SET NOT NULL;

ALTER TABLE t_m.tasks
ALTER COLUMN status_id SET NOT NULL;

create index if not exists idx_task_title
on t_m.tasks (subject);

create index if not exists idx_user_id
on t_m.tasks (owner_id);

create index if not exists idx_assign_id
on t_m.tasks (assigned_to_id);

create index if not exists idx_project_id
on t_m.tasks (project_id);

create index if not exists idx_status_task_id
on t_m.tasks (status_id);


create table if not exists t_m.project_responsible (
	project_id bigint,
	id_job_title int,
	user_id int,
	constraint con_pro_res FOREIGN KEY (project_id) REFERENCES t_m.projects(project_id) ON DELETE restrict,
	constraint con_job_res FOREIGN KEY (id_job_title) REFERENCES t_m.job_titles(id_job_title) ON DELETE restrict,
	constraint con_us_res FOREIGN KEY (user_id) REFERENCES t_m.users(user_id) ON DELETE restrict
);
	
create index if not exists idx_project_id_res
on t_m.project_responsible (project_id);

create index if not exists idx_job_title_res
on t_m.project_responsible (id_job_title);

create index if not exists idx_user_id_res
on t_m.project_responsible (user_id);

create table if not exists t_m.comments (
	comment_id bigserial,
	description varchar(1000),
	issue_id bigint,
	project_id bigint,
	user_id int not null,
	created_date timestamp DEFAULT current_timestamp,
	constraint com_id primary key (comment_id),
	constraint pr_id_con_com FOREIGN KEY (project_id) REFERENCES t_m.projects(project_id) ON DELETE restrict,
	constraint task_id_con_com FOREIGN KEY (issue_id) REFERENCES t_m.tasks(issue_id) ON DELETE restrict,
	constraint user_id_con_com FOREIGN KEY (user_id) REFERENCES t_m.users(user_id) ON DELETE restrict
);

create index if not exists id_com_task
on t_m.comments(issue_id);

create index if not exists id_com_user
on t_m.comments (user_id);

create index if not exists id_com_project
on t_m.comments (project_id);

