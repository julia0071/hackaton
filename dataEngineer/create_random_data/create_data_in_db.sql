INSERT INTO t_m.job_titles (name_job_title)
VALUES
    ('Проектный менеджер'),
    ('Аналитик'),
    ('Тестировщик'),
    ('Программист'),
    ('Инженер');
	
INSERT INTO t_m.locations (country, city)
VALUES
    ('Россия', 'Москва'),
    ('Россия', 'Санкт-Петербург'),
    ('Россия', 'Воронеж'),
    ('Россия', 'Саратов'),
    ('Россия', 'Пенза'),
	('Россия', 'Калининград'),
	('Россия', 'Череповец');
	
INSERT INTO t_m.statuses (status_name)
VALUES
    ('Create'),
    ('In_progress'),
    ('Close'),
	('Canceled');
	
INSERT INTO t_m.departments (name_department)
VALUES
    ('Департамент разработки ERP-систем'),
    ('Департамент разработки систем распознавания речи'),
    ('Департамент разработки биометрических систем'),
	('Департамент разработки поисковых систем'),
	('Департамент разработки систем сбора данных');




