
from faker import Faker
from random import randint, choice
from datetime import datetime
from conn import connection
import psycopg2


cursor = connection.cursor()

fake = Faker('ru_RU')

cursor.execute("SELECT id_job_title FROM t_m.job_titles")
job_titles = [row[0] for row in cursor.fetchall()]

cursor.execute("SELECT department_id FROM t_m.departments")
departments = [row[0] for row in cursor.fetchall()]

cursor.execute("SELECT location_id FROM t_m.locations")
locations = [row[0] for row in cursor.fetchall()]

roles = ['ROLE_ADMIN', 'ROLE_PROJECT_MANAGER', 'ROLE_USER']

for _ in range(300): 
    first_name = fake.first_name()
    last_name = fake.last_name()
    second_name = None
    email = fake.email()
    password = fake.password()
    department_id = choice(departments)
    id_job_title = choice(job_titles)
    location_id = choice(locations)
    telephone = fake.phone_number()
    created_date = fake.date_time_between(start_date="-1y", end_date="now")
    refresh_token = fake.pystr(min_chars=30, max_chars=30),
    expiration_date_token = fake.date_time_between(start_date="now", end_date="+1y"),
    verify_code = fake.pyint(min_value=100000, max_value=999999),
    verify_status = True,
    name_role = choice(roles)

    insert_query = """
    INSERT INTO t_m.users
    (first_name, last_name, second_name, email, password, department_id, id_job_title, location_id, 
        telephone, created_date, refresh_token, expiration_date_token,  verify_code, verify_status, name_role)
    VALUES
    (%s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s)
    """
    cursor.execute(insert_query, (
        first_name, last_name, second_name, email, password, department_id, id_job_title, location_id, 
        telephone, created_date, refresh_token, expiration_date_token,  verify_code, verify_status, name_role
    ))

connection.commit()

cursor.close()
connection.close()