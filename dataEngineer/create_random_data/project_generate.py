
from faker import Faker
from random import randint, choice
from datetime import datetime
from conn import connection
import psycopg2
import random
from datetime import timedelta
from random import randrange


cursor = connection.cursor()

fake = Faker('ru_RU')

cursor.execute("SELECT user_id FROM t_m.users WHERE name_role='ROLE_PROJECT_MANAGER'")
owner = [row[0] for row in cursor.fetchall()]

#owner = [2,10,14,17]

status_id = [1,2,4]

project_type = ['Investment', 'Custom', 'Support']

possible_values = ["value1", "value2", None]
selected_value = random.choice(possible_values)
current_date = datetime.now()
start_date = current_date - timedelta(days=90)

for _ in range(25): 
    project_title = fake.catch_phrase()
    goal = fake.catch_phrase()
    start_plan_date = start_date + timedelta(days=randrange(90))
 #   start_plan_date = fake.date_time_this_decade()
    selected_value = random.choice(["value1", "value2", None])
    
    if selected_value is None:
        start_on = None
        closed_on = None
        estimate_time_plan = f"{random.randint(1, 30)}d"
        end_plan_date = start_plan_date + timedelta(days=int(estimate_time_plan[:-1]))
        estimate_time_fact = None
    else:
        start_on = fake.date_time_between(start_date=start_plan_date)
        estimate_time_plan = f"{random.randint(1, 30)}d"
        estimate_time_fact = f"{random.randint(1, 30)}d"
        end_plan_date = start_plan_date + timedelta(days=int(estimate_time_plan[:-1]))
        closed_on = start_on + timedelta(days=int(estimate_time_fact[:-1]))

    project_description = fake.text()
    if closed_on is not None:
        status_id = 3
    else:
        status_id = 2
    project_description = fake.catch_phrase()
    owner_id = choice(owner)
    project_type_name = choice(project_type)
    created_on = fake.date_time_this_decade()



    insert_query = """
    INSERT INTO t_m.projects
    (project_title, goal, start_plan_date, start_on, end_plan_date, closed_on,
            estimate_time_plan, estimate_time_fact, project_description, status_id, owner_id,
            project_type_name, created_on)
    VALUES
    (%s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s)
    """
    cursor.execute(insert_query, (
        project_title, goal, start_plan_date, start_on, end_plan_date, closed_on,
            estimate_time_plan, estimate_time_fact, project_description, status_id, owner_id,
            project_type_name, created_on
    ))

connection.commit()

cursor.close()
connection.close()