
from faker import Faker
from random import randint, choice
from datetime import datetime
from conn import connection
import psycopg2
import random
from datetime import timedelta


cursor = connection.cursor()

fake = Faker('ru_RU')

cursor.execute("""
    SELECT
        p.project_id,
        jt.id_job_title,
        u.user_id
    FROM
        t_m.projects p
    JOIN
        t_m.job_titles jt ON TRUE
    JOIN
        t_m.users u ON TRUE
    ORDER BY
        random()
    LIMIT 100  -- Выберите количество записей для вставки
""")

records = cursor.fetchall()

for record in records:
    project_id, job_title_id, user_id = record
    cursor.execute("""
        INSERT INTO t_m.project_responsible
        (project_id, id_job_title, user_id)
        VALUES
        (%s, %s, %s)
    """, (project_id, job_title_id, user_id))


connection.commit()

cursor.close()
connection.close()