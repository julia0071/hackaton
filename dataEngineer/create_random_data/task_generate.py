
from faker import Faker
from random import randint, choice
from datetime import datetime
from conn import connection
import psycopg2
import random
from datetime import timedelta
from random import randrange


cursor = connection.cursor()

fake = Faker('ru_RU')

cursor.execute("SELECT user_id FROM t_m.users")
user_id = [row[0] for row in cursor.fetchall()]

cursor.execute("SELECT project_id FROM t_m.projects")
project = [row[0] for row in cursor.fetchall()]

status_id = [1,2,4]

task_type = ['Task', 'Epic', 'Story', 'Bug']
priority = ['High','Middle','Low']

possible_values = ["value1", "value2", None]
selected_value = random.choice(possible_values)
current_date = datetime.now()
start_date = current_date - timedelta(days=90)

for _ in range(200): 
    subject = fake.catch_phrase()
    description = fake.catch_phrase()
    start_plan_date = start_date + timedelta(days=randrange(90))
    selected_value = random.choice(["value1", "value2", None])
    
    if selected_value is None:
        start_on = None
        closed_on = None
        estimate_time_plan = f"{random.randint(1, 30)}d"
        end_plan_date = start_plan_date + timedelta(days=int(estimate_time_plan[:-1]))
        estimate_time_fact = None
    else:
        start_on = fake.date_time_between(start_date=start_plan_date)
        estimate_time_plan = f"{random.randint(1, 30)}d"
        estimate_time_fact = f"{random.randint(1, 30)}d"
        end_plan_date = start_plan_date + timedelta(days=int(estimate_time_plan[:-1]))
        closed_on = start_on + timedelta(days=int(estimate_time_fact[:-1]))

    project_description = fake.catch_phrase()
    if closed_on is not None:
        status_id = 3
    else:
        status_id = 2
    owner_id = choice(user_id)
    assigned_to_id = choice(user_id)
    project_id = random.choice(project)
    task_type_name  = choice(task_type)
    created_on = fake.date_time_this_decade()
    priority_name = random.choice(priority)
    parent_id=None



    insert_query = """
    INSERT INTO t_m.tasks
    (subject, description, task_type_name, start_plan_date, start_on, end_plan_date, closed_on,
    estimate_time_plan, estimate_time_fact, owner_id, assigned_to_id, parent_id, project_id, priority_name, status_id, created_on)
    VALUES
    (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """
    cursor.execute(insert_query, (
    subject, description, task_type_name, start_plan_date, start_on, end_plan_date, closed_on,
    estimate_time_plan, estimate_time_fact, owner_id, assigned_to_id, parent_id, project_id, priority_name, status_id, created_on
    ))

connection.commit()

cursor.close()
connection.close()