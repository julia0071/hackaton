
from faker import Faker
from random import randint, choice
from datetime import datetime
from conn import connection
import psycopg2
import random
from datetime import timedelta


cursor = connection.cursor()

fake = Faker('ru_RU')

cursor.execute("SELECT issue_id FROM t_m.tasks")
issue_ids = [row[0] for row in cursor.fetchall()]

cursor.execute("SELECT project_id FROM t_m.projects")
project_ids = [row[0] for row in cursor.fetchall()]

cursor.execute("SELECT user_id FROM t_m.users")
user_ids = [row[0] for row in cursor.fetchall()]

for _ in range(500):  
    description = fake.text(max_nb_chars=1000)
    issue_id = random.choice(issue_ids)
    project_id = random.choice(project_ids)
    user_id = random.choice(user_ids)
    created_date = fake.date_time_between(start_date="-90d", end_date="now")

 
    insert_query = """
    INSERT INTO t_m.comments
    (description, issue_id, project_id, user_id, created_date)
    VALUES
    (%s, %s, %s, %s, %s)
    """
    cursor.execute(insert_query, (description, issue_id, project_id, user_id, created_date))

connection.commit()

cursor.close()
connection.close()