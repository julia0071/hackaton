create schema if not exists dm;


create table if not exists dm.response_status_project(
   project_id int,
   project_title varchar,
   status_id int,
   start_plan_date timestamp,
   end_plan_date timestamp,
   start_on timestamp,
   closed_on timestamp,
   gap_start interval,
   gap_end interval);

create table if not exists dm.response_status_task(
   issue_id int,
   project_id int,
   subject varchar,
   status_id int,
   start_plan_date timestamp,
   end_plan_date timestamp,
   start_on timestamp,
   closed_on timestamp,
   gap_start interval,
   gap_end interval);

create table if not exists dm.response_user(
   user_id int,
   first_name varchar,
   last_name varchar,
   task_count int,
   total_estimate_time int);
   
create table if not exists dm.response_project(
   project_id int,
   project_title varchar,
   task_count int,
   total_estimate_time int);
   
