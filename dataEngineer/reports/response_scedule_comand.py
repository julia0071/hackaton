
import psycopg2
import schedule
import time
from conn import connection


def execute_query(connection):
    try:
        cursor = connection.cursor()

        sql_query = """
        DELETE FROM dm.response_project;
        INSERT INTO dm.response_project(
        project_id,
        project_title,
        task_count,
        total_estimate_time)
        SELECT
        pr.project_id,
        pr.project_title,
        COUNT(tk.issue_id) AS task_count,
        SUM(CAST(SUBSTRING(tk.estimate_time_fact, '(\d+)') AS INTEGER)) AS total_estimate_time
        FROM
        t_m.projects AS pr
        LEFT JOIN
        t_m.tasks AS tk ON pr.project_id = tk.project_id
        AND tk.closed_on BETWEEN '2023-10-01' AND '2023-10-15'
        GROUP BY
        pr.project_id,
        pr.project_title;
        """

        cursor.execute(sql_query)
        connection.commit()
        cursor.close()
        connection.close()
        print("Запрос выполнен успешно.")
    except Exception as e:
        print(f"Ошибка выполнения запроса: {e}")

schedule.every().day.at("15:07").do(execute_query, connection)

while True:
    schedule.run_pending()
    time.sleep(1)

        

