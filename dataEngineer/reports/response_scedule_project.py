
import psycopg2
import schedule
import time
from conn import connection


def execute_query(connection):
    try:
        cursor = connection.cursor()

        sql_query = """
        DELETE FROM dm.response_status_project;
        INSERT INTO dm.response_status_project (project_id, project_title, status_id, start_plan_date, end_plan_date, start_on, closed_on, gap_start, gap_end)
        SELECT project_id, project_title, status_id, start_plan_date, end_plan_date, start_on, closed_on, (start_plan_date - start_on) as Gap_start, (end_plan_date - closed_on) as Gap_end
        FROM t_m.projects;
        """

        cursor.execute(sql_query)
        connection.commit()
        cursor.close()
        connection.close()
        print("Запрос выполнен успешно.")
    except Exception as e:
        print(f"Ошибка выполнения запроса: {e}")

schedule.every().day.at("15:01").do(execute_query, connection)

while True:
    schedule.run_pending()
    time.sleep(1)

        

