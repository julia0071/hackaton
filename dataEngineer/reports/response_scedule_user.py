
import psycopg2
import schedule
import time
from conn import connection


def execute_query(connection):
    try:
        cursor = connection.cursor()

        sql_query = """
        DELETE FROM dm.response_user;
        INSERT INTO dm.response_user (user_id,first_name,last_name,task_count,total_estimate_time)
        SELECT
        all_ud.user_id,
        all_ud.first_name,
        all_ud.last_name,
        COUNT(tk.issue_id) AS task_count,
        SUM(CAST(SUBSTRING(tk.estimate_time_fact, '(\d+)') AS INTEGER)) AS total_estimate_time
        FROM
        t_m.users AS all_ud
        LEFT JOIN
        t_m.tasks AS tk ON all_ud.user_id = tk.assigned_to_id
        AND closed_on BETWEEN '2023-10-01' AND '2023-10-15'
        GROUP BY
        all_ud.user_id, all_ud.first_name, all_ud.last_name;
        """

        cursor.execute(sql_query)
        connection.commit()
        cursor.close()
        connection.close()
        print("Запрос выполнен успешно.")
    except Exception as e:
        print(f"Ошибка выполнения запроса: {e}")

schedule.every().day.at("15:05").do(execute_query, connection)

while True:
    schedule.run_pending()
    time.sleep(1)

        

