
import psycopg2
import schedule
import time
from conn import connection


def execute_query(connection):
    try:
        cursor = connection.cursor()

        sql_query = """
        DELETE FROM dm.response_status_task;
        INSERT INTO dm.response_status_task (issue_id, project_id, subject, status_id, start_plan_date, end_plan_date, start_on, closed_on, gap_start,gap_end)
        select task.issue_id, pr.project_id, task.subject, task.status_id, task.start_plan_date, task.end_plan_date, task.start_on, task.closed_on, (task.start_plan_date-task.start_on) as Gap_start,(task.end_plan_date-task.closed_on) as Gap_end
        from t_m.tasks as task inner join t_m.projects as pr
        on task.project_id = pr.project_id;
        """

        cursor.execute(sql_query)
        connection.commit()
        cursor.close()
        connection.close()
        print("Запрос выполнен успешно.")
    except Exception as e:
        print(f"Ошибка выполнения запроса: {e}")

schedule.every().day.at("15:03").do(execute_query, connection)

while True:
    schedule.run_pending()
    time.sleep(1)

        

