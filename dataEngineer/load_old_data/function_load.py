from sqlalchemy import create_engine
import pandas as pd
from log import log_action
from datetime import datetime
from conn import connection

connection_string = connection

def call_function(connection_string):
    start_time = datetime.now()
    engine = create_engine(connection_string)

    csv_file = 'task_all_corr.csv'


    try:
        with open(csv_file, 'r') as file:
            df = pd.read_csv(csv_file, sep=',', encoding='utf-8')
            df.to_sql('tasks', engine, schema = 'archive', if_exists='append', index=False)
            end_time=datetime.now()
            log_action(
                start_time,
                end_time,
                source='Row_data',
                success=True,
                error_code=None,
                action_task='load'
            )
    except Exception as e:
        error_message = f'Error during data loading: {str(e)}'
        end_time=datetime.now()
        log_action(
                start_time,
                end_time,
                source='Row_data',
                success=False,
                error_code=error_message,
                action_task='load'
            )
        raise

call_function(connection_string)