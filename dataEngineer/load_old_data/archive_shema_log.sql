create schema if not exists logs;
create schema if not exists archive;


create table archive.tasks (
issue_id int,
created_on timestamp,
closed_on timestamp,
status_id int,
subject varchar(300),
description  varchar (3000),
assigned_to_id int,
parent_id int,
project_id int,
constraint pr_key primary key(issue_id)
);