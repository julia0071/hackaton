const ESLintPlugin = require("eslint-webpack-plugin");

module.exports = {
  mode: "development",
  devtool: "cheap-module-source-map",
  plugins: [
    new ESLintPlugin({
      context: "../",
      emitError: true,
      emitWarning: true,
      failOnError: true,
      extensions: ["ts", "tsx"]
    })
  ],
  devServer: {
    port: 8081,
    historyApiFallback: true,
    // proxy: {
    //   "/registration": {
    //     target: "localhost:8080/registration",
    //     secure: false,
    //     changeOrigin: true,
    //     headers: {
    //       "Access-Control-Allow-Origin": "*",
    //       "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
    //       "Access-Control-Allow-Headers":
    //         "X-Requested-With, content-type, Authorization"
    //     }
    //   },
    //   "/auth": {
    //     target: "localhost:8080/auth",
    //     secure: false,
    //     changeOrigin: true,
    //     headers: {
    //       "Access-Control-Allow-Origin": "*",
    //       "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
    //       "Access-Control-Allow-Headers":
    //         "X-Requested-With, content-type, Authorization"
    //     }
    //   }
    // },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers":
        "X-Requested-With, content-type, Authorization"
    }
  }
};
