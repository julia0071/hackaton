const { merge } = require("webpack-merge");
const commonConfig = require("./webpack.common.ts");

interface IProcessEnv {
  [key: string]: string | undefined
}

module.exports = (envVars: IProcessEnv) => {
  const { env } = envVars;
  const envConfig = require(`./webpack.${env}.ts`);
  const config = merge(commonConfig, envConfig);
  return config;
};
