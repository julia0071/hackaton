const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
require("dotenv").config({path: '../.env'})

module.exports = {
  entry: path.resolve(__dirname, "..", "./src/index.tsx"),
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
    alias: {
      "@": path.resolve(__dirname, "..", "./src")
    }
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader"
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          "css-loader",
          {
            loader: "sass-loader",
            options: {
              additionalData:
                "@import '@/assets/scss/vars.scss';@import '@/assets/scss/mixins.scss';@import '@/assets/scss/functions.scss';"
            }
          }
        ]
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, "..", "./build"),
    filename: "bundle.js",
    publicPath: "/",
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "..", "./src/index.html")
    }),
    new webpack.DefinePlugin({
      "process.env": {
        BASE_HOST: JSON.stringify(process.env.BASE_HOST),
        BASE_PORT: JSON.stringify(process.env.BASE_PORT)
      }
    }),
  ]
};
