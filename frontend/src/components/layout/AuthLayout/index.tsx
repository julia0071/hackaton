import { Layout } from "antd";
import "./style.scss";

interface PropTypes {
  children?: React.ReactNode;
}

const AuthLayout = ({ children }: PropTypes) => {
  return (
    <Layout className="auth-layout site-layout">
      <Layout.Content className="auth-layout__content">{children}</Layout.Content>
    </Layout>
  );
};

export default AuthLayout;
