import { type ProjectStatusType } from "@/types/Project";
import { type TaskPriorityType } from "@/types/Task";
import { Tag, type TagProps } from "antd";

type PropTypes = {
  type: ProjectStatusType | TaskPriorityType;
  size: "small" | "default";
  text: string;
} & TagProps;

const StatusTag = ({ text, type, size = "default" }: PropTypes) => {
  enum StatusColor {
    "Высокий" = "red",
    "Средний" = "gold",
    "Низкий" = "green",
    "Создан" = "blue",
    "В работе" = "processing",
    "Закрыт" = "default",
    "Отменён" = "red"
  }
  return (
    <Tag color={StatusColor[text]} bordered={false} className="status-tag">
      {text}
    </Tag>
  );
};

export default StatusTag;
