import { LoadingOutlined } from "@ant-design/icons";
import { Spin, type SpinProps } from "antd";
import "./style.scss";

const Spinner = ({ ...props }: SpinProps) => {
  return (
      <Spin className="spinner" indicator={<LoadingOutlined spin />} size="large" {...props}/>
  );
};

export default Spinner;
