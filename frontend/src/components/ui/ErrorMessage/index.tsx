import "./style.scss";

interface PropTypes {
  text?: string;
}

const ErrorMessage = ({
  text = "Sorry, there was an error processing your request."
}: PropTypes) => {
  return <p className="error-message">{text}</p>;
};

export default ErrorMessage;
