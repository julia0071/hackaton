import { firstStringLetters } from "@/utils/string";
import { Avatar as AntAvatar, type AvatarProps } from "antd";
import uniqolor from "uniqolor";

type PropTypes = {
  name: string;
  lastName: string;
} & AvatarProps;

const Avatar = ({ name, lastName, ...props }: PropTypes) => {
  const initials = firstStringLetters([name, lastName]);
  const randomColor = uniqolor(initials);
  return (
    <AntAvatar style={{ backgroundColor: randomColor.color }} className="avatar" {...props}>
      {initials}
    </AntAvatar>
  );
};

export default Avatar;
