import { Avatar as AntAvatar } from "antd";

import type { IUser } from "@/types/Users";
import { firstStringLetters } from "@/utils/string";
import { AvatarGroupMaxStyles } from "./style";

interface PropTypes {
  users?: IUser[];
}

const mockUsers = [
  {
    userId: 1,
    name: "Mic",
    lastName: "L",
    secondName: "M",
    email: "M"
  },
  {
    userId: 2,
    name: "John",
    lastName: "Dohn",
    secondName: "M",
    email: "M"
  },
  {
    userId: 3,
    name: "John",
    lastName: "Dohn",
    secondName: "M",
    email: "M"
  },
  {
    userId: 4,
    name: "Mic",
    lastName: "L",
    secondName: "M",
    email: "M"
  }
];

const AvatarGroup = ({ users = mockUsers }: PropTypes) => {
  return (
    <AntAvatar.Group maxCount={3} size="small" maxStyle={AvatarGroupMaxStyles}>
      {users.map(({ userId, name, lastName }) => {
        const initials = firstStringLetters([name, lastName]);
        return <AntAvatar key={userId}>{initials}</AntAvatar>;
      })}
    </AntAvatar.Group>
  );
};

export default AvatarGroup;
