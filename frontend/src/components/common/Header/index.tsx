import { Layout } from "antd";
import { Link } from "react-router-dom";
import Avatar from "@/components/ui/Avatar";

import "./style.scss";
import { useAppSelector } from "@/hooks/useAppSelector";

const Header = () => {
  const user = useAppSelector((state) => state.user.current);
  return (
    <Layout.Header className="header">
      <Link to="/" className="header__logo">
        NeoHack
      </Link>
      <Link to="/dashboard" className="header__user">
        {user && (
          <>
            <div className="header__user-info">
              <span className="header__user-name">
                {`${user.firstName} ${user.lastName}`}
              </span>
              <span className="header__user-desc">{user.jobTitle}</span>
            </div>
            <Avatar
              className="header__avatar"
              name={user.firstName}
              lastName={user.lastName}
            />
          </>
        )}
      </Link>
    </Layout.Header>
  );
};

export default Header;
