import AuthLayout from "@/components/layout/AuthLayout";
import { useAppDispatch } from "@/hooks/useAppDispatch";
import { type IAuthRequest } from "@/types/Auth";
import { Alert, Button, Form, Input } from "antd";
import { Link, useNavigate } from "react-router-dom";
import "./style.scss";
import { useAppSelector } from "@/hooks/useAppSelector";
import { LoadingOutlined } from "@ant-design/icons";
import { login } from "@/store/slices/authSlice";

const Auth = () => {
  const [form] = Form.useForm();

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const status = useAppSelector((state) => state.auth.status);

  const onFinish = async (values: IAuthRequest) => {
    try {
      await dispatch(login(values)).unwrap();
      navigate("/dashboard");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <AuthLayout>
      <h2 className="auth__title">Добро пожаловать!</h2>
      <span className="auth__registration-text">
        У вас еще нет аккаунта?&nbsp;
        <Link to="/registration" className="auth__registration-link">
          Зарегистрируйтесь
        </Link>
      </span>
      <Form
        name="auth"
        className="auth"
        form={form}
        onFinish={onFinish}
        autoComplete="off"
        layout="vertical"
      >
        <Form.Item
          name="email"
          rules={[
            { required: true, message: "Введите e-mail" },
            { type: "email", message: "Введите корректный e-mail" }
          ]}
        >
          <Input
            bordered={false}
            placeholder="E-mail"
            className="auth__input"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            { required: true, message: "Введите пароль" },
            { min: 6, message: "Пароль должен содержать не менее 6-х символов" }
          ]}
        >
          <Input.Password
            bordered={false}
            placeholder="Пароль"
            className="auth__input"
          />
        </Form.Item>
        <Form.Item>
          <Button
            htmlType="submit"
            className="auth__btn"
            type="primary"
            disabled={status === "loading"}
          >
            {status === "loading" ? <LoadingOutlined spin /> : "Войти"}
          </Button>
        </Form.Item>
        {status === "error" && (
          <Alert type="error" showIcon message="Ошибка сервера" />
        )}
      </Form>
    </AuthLayout>
  );
};

export default Auth;
