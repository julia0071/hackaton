// import Kanban from "@/components/common/Kanban";
import TasksHeader from "./TasksHeader";
import TasksList from "./TasksList";
import { useState } from "react";
import { type TasksViewType } from "@/types/Task";

const Tasks = () => {
  const [view, setView] = useState<TasksViewType>("kanban");
  return (
    <>
      <TasksHeader
        view={view}
        changeView={(view) => setView(view)}
      />
      {/* {view === "kanban" && <Kanban />} */}
      {view === "list" && <TasksList />}
    </>
  );
};

export default Tasks;
