import { Button, Input, Select } from "antd";
import { Link } from "react-router-dom";
import { type TasksViewType } from "@/types/Tasks";
import "./style.scss";

interface PropTypes {
  view: TasksViewType;
  changeView: (value: React.SetStateAction<TasksViewType>) => void;
}

const TasksHeader = ({ view, changeView }: PropTypes) => {
  const handleChange = (data: TasksViewType) => {
    changeView(data);
  };
  return (
    <div className="tasks-header">
      <Input.Search />
      <Select
      className="tasks-header__select"
        defaultValue={view}
        onChange={handleChange}
        options={[
          { value: "kanban", label: "Kanban" },
          { value: "list", label: "List" }
        ]}
      />
      <Link to="create" className="tasks-header__button">
        <Button type="default">Create task</Button>
      </Link>
    </div>
  );
};

export default TasksHeader;
