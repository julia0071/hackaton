// import Spinner from "@/components/ui/Spinner";
import Task from "@/components/common/Task";
import ErrorMessage from "@/components/ui/ErrorMessage";
import "./style.scss";
import { useAppSelector } from "@/hooks/useAppSelector";

const TasksList = () => {
  const status = useAppSelector((state) => state.auth.status);
  const tasks = useAppSelector((state) => state.tasks.items);
  const tasksList = Object.values(tasks).flat();
  return (
    <>
      {/* <Spinner spinning={status === "loading"} /> */}
      {/* {status === "resolved" && ( */}
      <ul className="tasks-list">
        {tasksList.map((item) => {
          return <Task key={item.id} {...item} />;
        })}
      </ul>
      {/* )} */}
      {status === "error" && <ErrorMessage />}
    </>
  );
};

export default TasksList;
