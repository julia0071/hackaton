import { Tag, Typography } from "antd";
import { FolderOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import IconText from "@/components/ui/IconText";
import { type IProject, ProjectStatusEnum } from "@/types/Project";
import "./style.scss";

const ProjectDetail = ({
  projectId,
  projectTitle,
  projectDescription,
  tasks,
  projectStatus
}: IProject) => {
  return (
    <div className="project-item">
      <div className="project-item__header">
        <Link key={projectId} to={`/projects/${projectId}`}>
          <Typography.Title level={3}>{projectTitle}</Typography.Title>
        </Link>
        <Tag color="default" className="project-item__tag">
          {ProjectStatusEnum[projectStatus]}
        </Tag>
      </div>
      <p className="project-item__description">{projectDescription}</p>
      <div className="project-item__bottom">
        <IconText icon={<FolderOutlined />} text={`${tasks?.length ?? 0} задач`} />
      </div>
    </div>
  );
};

export default ProjectDetail;
