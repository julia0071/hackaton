import "./style.scss";
import { Button } from "antd";
import { Link } from "react-router-dom";
import ProjectItem from "../ProjectItem";
import { type ProjectsType } from "@/types/Project";

interface PropTypes {
  projects: ProjectsType;
}

const ProjectsList = ({ projects }: PropTypes) => {
  return (
    <>
      <h2 style={{ fontWeight: 400 }}>Проекты</h2>
      <div className="tasks-header">
        <Link to="create" className="tasks-header__button">
          <Button type="default">Создать проект</Button>
        </Link>
      </div>
      <section className="projects-list">
        {projects.map((project) => {
          return <ProjectItem key={project.projectId} {...project} />;
        })}
      </section>
    </>
  );
};

export default ProjectsList;
