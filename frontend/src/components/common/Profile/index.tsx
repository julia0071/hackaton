/* eslint-disable @typescript-eslint/no-unused-vars */
import { Alert, Divider } from "antd";
import "./style.scss";
import IconText from "../../ui/IconText";
import {
  MailOutlined,
  PhoneOutlined,
  TeamOutlined,
  UserOutlined
} from "@ant-design/icons";
import { useAppDispatch } from "@/hooks/useAppDispatch";
import { useEffect, useState } from "react";
import { getUserById } from "@/store/slices/userSlice";
import { useAppSelector } from "@/hooks/useAppSelector";
import { type StatusType } from "@/types/Status";
import Spinner from "@/components/ui/Spinner";
import Avatar from "@/components/ui/Avatar";
import { Link } from "react-router-dom";
import ContentLayout from "@/components/layout/ContentLayout";
import TasksList from "../Tasks/TasksList";

const Profile = () => {
  const [status, setStatus] = useState<StatusType>("resolved");
  const dispatch = useAppDispatch();
  const id = useAppSelector((state) => state.auth.user);
  const user = useAppSelector((state) => state.user.current);

  // useEffect(() => {
  //   dispatch(getUserById(Number(id)))
  //     .unwrap()
  //     .then(() => {
  //       setStatus("resolved");
  //     })
  //     .catch(() => {
  //       setStatus("error");
  //     });
  // }, [dispatch]);
  const handleLogout = () => {};
  return (
    <>
      {status === "loading" && <Spinner />}
      {status === "error" && (
        <Alert type="error" showIcon message="Ошибка сервера" />
      )}
      {status === "resolved" && user && (
        <div className="profile">
          <div className="user-info">
            <Avatar
              name={user.firstName}
              lastName={user.lastName}
              size="large"
              className="user-info__pic"
            />
            <ul className="user-info__list">
              <li>
                <b>
                  {user.firstName} {user.lastName}
                </b>
              </li>
              <li>{user.location.city}</li>
              <li>{user.location.country}</li>
            </ul>
            <Divider />
            <ul className="user-info__list user-info__list_icon">
              <li>
                <IconText icon={<UserOutlined />} text={user.jobTitle} />
              </li>
              <li>
                <IconText icon={<PhoneOutlined />} text={user.telephone} />
              </li>
              <li>
                <IconText icon={<MailOutlined />} text={user.email} />
              </li>
              <li>
                <IconText icon={<TeamOutlined />} text={user.department} />
              </li>
            </ul>
            <div className="user-info__links">
              <span
                onClick={handleLogout}
                style={{ color: "red", cursor: "pointer" }}
              >
                Выйти
              </span>
              <Link to="/user/edit" style={{ color: "#5079ad" }}>
                Изменить
              </Link>
            </div>
          </div>
          <div className="current-tasks">
            <h2 style={{ fontWeight: 400 }}>Мои задачи</h2>
            <TasksList />
          </div>
        </div>
      )}
    </>
  );
};

export default Profile;
