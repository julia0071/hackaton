import AuthLayout from "@/components/layout/AuthLayout";
import { Alert, Button, Form, Input } from "antd";
import { Link } from "react-router-dom";
import { LoadingOutlined } from "@ant-design/icons";
import { type IRegisterRequest } from "@/types/Register";
import userApi from "@/api/user.api";
import { type StatusType } from "@/types/Status";
import { useState } from "react";

const Registration = () => {
  const [form] = Form.useForm()
  const [status, setStatus] = useState<StatusType>("start");
  const onFinish = async (values: IRegisterRequest) => {
    setStatus("loading");
    try {
      await userApi.register(values);
      setStatus("resolved");
    } catch (error) {
      setStatus("error");
    }
  };

  return (
    <AuthLayout>
      <h2 className="auth__title">Создать аккаунт</h2>
      <p className="auth__subtitle">
        Создайте аккаунт для доступа к проектам и задачам
      </p>
      <Form
        className="auth"
        name="auth"
        form={form}
        onFinish={onFinish}
        autoComplete="off"
        layout="vertical"
        initialValues={{
          surName: null
        }}
      >
        <Form.Item
          name="email"
          rules={[
            { required: true, message: "Введите e-mail" },
            { type: "email", message: "Введите корректный e-mail" }
          ]}
        >
          <Input
            bordered={false}
            placeholder="E-mail"
            className="auth__input"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            { required: true, message: "Введите пароль" },
            {
              min: 6,
              message: "Пароль должен содержать не менее 6-ти символов"
            }
          ]}
        >
          <Input.Password
            bordered={false}
            placeholder="Пароль"
            className="auth__input"
          />
        </Form.Item>
        <Form.Item
          name="name"
          rules={[
            { required: true, message: "Введите имя" },
            { min: 3, message: "Имя должно содержать не менее 3-х символов" }
          ]}
        >
          <Input bordered={false} placeholder="Имя" className="auth__input" />
        </Form.Item>
        <Form.Item
          name="lastName"
          rules={[
            { required: true, message: "Введите фамилию" },
            {
              min: 3,
              message: "Фамилия должна содержать не менее 3-х символов"
            }
          ]}
        >
          <Input
            bordered={false}
            placeholder="Фамилия"
            className="auth__input"
          />
        </Form.Item>
        <Form.Item name="secondName">
          <Input
            bordered={false}
            placeholder="Отчество"
            className="auth__input"
          />
        </Form.Item>
        <Form.Item>
          <Button
            htmlType="submit"
            className="auth__btn"
            type="primary"
            disabled={status === "loading"}
          >
            {status === "loading" ? (
              <LoadingOutlined spin />
            ) : (
              "Создать аккаунт"
            )}
          </Button>
          <span className="auth__registration-text">
            Уже есть аккаунт?&nbsp;
            <Link to="/auth" className="auth__registration-link">
              Войти
            </Link>
          </span>
        </Form.Item>
        {status === "error" && (
          <Alert type="error" showIcon message="Ошибка сервера" />
        )}
        {status === "resolved" && (
          <Alert type="success" showIcon message="Регистрация прошла успешно" />
        )}
      </Form>
    </AuthLayout>
  );
};

export default Registration;
