import { BASE_URL } from "@/api/config";
import LocalStorage from "@/services/localStorage";
import {
  type IAuthRequest,
  type AuthStateType,
  type IAuthResponse
} from "@/types/Auth";
import {
  type PayloadAction,
  createSlice,
  createAsyncThunk
} from "@reduxjs/toolkit";
import axios, { type AxiosError, type AxiosResponse } from "axios";

const initialState: AuthStateType = {
  status: "start",
  user: 1,
  role: "ROLE_ADMIN"
};

export const login = createAsyncThunk(
  "auth/login",
  async function (authData: IAuthRequest, { rejectWithValue }) {
    try {
      const response = await axios.post("/auth", authData, { baseURL: BASE_URL });
      if (response.status !== 200) {
        throw new Error("Server Error!");
      }
      return response;
    } catch (error) {
      const err = error as AxiosError;
      return rejectWithValue(err.message);
    }
  }
);

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    logout: (state) => {
      LocalStorage.remove("accessToken");
      LocalStorage.remove("refreshToken");
      state = initialState;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(login.pending, (state) => {
        state.status = "loading";
      })
      .addCase(
        login.fulfilled,
        (state, action: PayloadAction<AxiosResponse<IAuthResponse>>) => {
          const { userId, accessToken, refreshToken, role } =
            action.payload.data;
          state.status = "resolved";
          state.user = userId;
          state.role = role;
          LocalStorage.set("accessToken", accessToken);
          LocalStorage.set("refreshToken", refreshToken);
        }
      )
      .addCase(login.rejected, (state) => {
        state.status = "error";
      });
  }
});

export const { logout } = authSlice.actions;

export default authSlice.reducer;
