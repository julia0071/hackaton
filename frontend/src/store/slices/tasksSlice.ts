import {
  type TaskStatusType,
  type TasksType,
  type ITask
} from "@/types/Task";
import { insertItemIntoArray, moveItemWithinArray } from "@/utils/common";
import { type PayloadAction, createSlice } from "@reduxjs/toolkit";
import { type DropResult } from "react-beautiful-dnd";

interface TasksStateType {
  items: TasksType;
}

const initialState: TasksStateType = {
  items: {
    Open: [
      {
        id: 1,
        title: "Управляемая и экоцентричная функциональность",
        description: "Настраиваемый и итернациональный параллелизм",
        status: "Create",
        priority: "High"
      }
    ],
    Process: [
      {
        id: 5,
        title: "Новая и радикальная парадигма",
        description: "Переосмысленная и целостная инициатива",
        status: "In_progress",
        priority: "Middle"
      },
      {
        id: 6,
        title: "Бизнес-ориентированное и мультимедийное взаимодействие",
        description: "Переключаемый и глобальный вызов",
        status: "In_progress",
        priority: "Middle"
      }
    ],
    Completed: [
      {
        id: 9,
        title: "Удобный и высокоуровневый адаптер",
        description: "Разнообразная и итернациональная поддержка",
        status: "In_progress",
        priority: "Middle"
      }
    ]
  }
};

const tasksSlice = createSlice({
  name: "tasks",
  initialState,
  reducers: {
    addTask: (state, action: PayloadAction<ITask>) => {
      state.items[action.payload.status].push(action.payload);
    },
    moveKanbanTask: (state, action: PayloadAction<DropResult>) => {
      const { source, destination, draggableId } = action.payload;
      if (destination && source) {
        const sourceColumn = source.droppableId as TaskStatusType;
        const destinationColumn = destination.droppableId as TaskStatusType;

        const draggableElem = state.items[sourceColumn].find(
          (el) => el.id === Number(draggableId)
        );
        if (draggableElem) {
          if (sourceColumn === destinationColumn) {
            state.items[sourceColumn] = moveItemWithinArray<ITask>(
              state.items[destinationColumn],
              draggableElem,
              destination.index
            );
          } else {
            draggableElem.status = destinationColumn;
            state.items[destinationColumn] = insertItemIntoArray<ITask>(
              state.items[destinationColumn],
              draggableElem,
              destination.index
            );
            state.items[sourceColumn] = state.items[sourceColumn].filter(
              (el) => el.id !== Number(draggableId)
            );
          }
        }
      }
    }
  }
});

export const { addTask, moveKanbanTask } = tasksSlice.actions;
export default tasksSlice.reducer;
