import api from "@/api/config";
import { type IUserInfo } from "@/types/Users";
import {
  type PayloadAction,
  createSlice,
  createAsyncThunk
} from "@reduxjs/toolkit";
import { type AxiosError, type AxiosResponse } from "axios";

interface IState {
  current: IUserInfo | null;
}
const initialState: IState = {
  //   current: null
  current: {
    userId: 0,
    jobTitle: "Разработчик",
    firstName: "Валентин",
    lastName: "Петров",
    email: "test@mail.ru",
    department: "Департамент разработки",
    telephone: "+77777777",
    createdDate: "string",
    location: {
      country: "Россия",
      city: "Москва"
    },
    role: "ROLE_ADMIN"
  }
};

export const getUserById = createAsyncThunk(
  "user/getUserById",
  async function (id: number, { rejectWithValue }) {
    try {
      const response = await api.get(`/user/${id}`);
      if (response.status !== 200) {
        throw new Error("Server Error!");
      }
      return response;
    } catch (error) {
      const err = error as AxiosError;
      return rejectWithValue(err.message);
    }
  }
);

const userSLice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(
      getUserById.fulfilled,
      (state, action: PayloadAction<AxiosResponse<IUserInfo>>) => {
        const user = action.payload.data;
        state.current = user;
      }
    );
  }
});

export default userSLice.reducer;
