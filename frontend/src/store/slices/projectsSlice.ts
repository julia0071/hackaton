import api from "@/api/config";
import { type IProject, type ProjectsType } from "@/types/Project";
import { type StatusType } from "@/types/Status";
import {
  type PayloadAction,
  createSlice,
  createAsyncThunk
} from "@reduxjs/toolkit";

const initialState: {
  status: StatusType;
  all: ProjectsType;
} = {
  status: "start",
  // all: []
  all: [
    {
      projectId: 7,
      projectTitle: "Многоканальная и широкая служба поддержки",
      projectType: "Custom",
      startDate: "2023-08-14",
      endDate: "2023-09-03",
      startDateFact: null,
      endDateFact: null,
      projectDescription: "Уменьшенное и специализированное шифрование",
      projectStatus: "Close",
      projectOwnerId: 119,
      comments: null,
      tasks: null,
      creationDate: null
    },
    {
      projectId: 10,
      projectTitle: "Многогранная и гибридная фокус-группа",
      projectType: "Support",
      startDate: "2023-08-04",
      endDate: "2023-08-16",
      startDateFact: null,
      endDateFact: null,
      projectDescription: "Корпоративная и однородная концепция",
      projectStatus: "In_progress",
      projectOwnerId: 296,
      comments: null,
      tasks: null,
      creationDate: null
    },
    {
      projectId: 16,
      projectTitle: "Интуитивный и объектно-ориентированный параллелизм",
      projectType: "Custom",
      startDate: "2023-07-28",
      endDate: "2023-08-18",
      startDateFact: null,
      endDateFact: null,
      projectDescription: "Общедоступная и пошаговая локальная сеть",
      projectStatus: "Close",
      projectOwnerId: 155,
      comments: null,
      tasks: null,
      creationDate: null
    },
    {
      projectId: 3,
      projectTitle: "Программируемый и контекстуальный системный движок",
      projectType: "Support",
      startDate: "2023-08-07",
      endDate: "2023-08-19",
      startDateFact: null,
      endDateFact: null,
      projectDescription:
        "Сетевой и веб-ориентированный графический интерфейс пользователя",
      projectStatus: "Close",
      projectOwnerId: 264,
      comments: null,
      tasks: null,
      creationDate: null
    },
    {
      projectId: 25,
      projectTitle: "Переосмысленное и экоцентричное приложение",
      projectType: "Investment",
      startDate: "2023-08-09",
      endDate: "2023-09-07",
      startDateFact: null,
      endDateFact: null,
      projectDescription: "Продвинутый и инструктивный подход",
      projectStatus: "In_progress",
      projectOwnerId: 139,
      comments: null,
      tasks: null,
      creationDate: null
    },
    {
      projectId: 19,
      projectTitle: "Распределённое и систематическое использование",
      projectType: "Support",
      startDate: "2023-09-05",
      endDate: "2023-10-05",
      startDateFact: null,
      endDateFact: null,
      projectDescription: "Полная и промежуточная конгломерация",
      projectStatus: "Close",
      projectOwnerId: 179,
      comments: null,
      tasks: null,
      creationDate: null
    }
  ]
};

export const getProjectsThunk = createAsyncThunk(
  "projects/getProjectsThunk",
  async () => {
    const response = await api.get("/projects");
    if (response.status !== 200) {
      throw new Error("Server Error!");
    }
    return response.data;
  }
);

export const deleteProjectThunk = createAsyncThunk(
  "projects/deleteProjectThunk",
  async (id: number, { dispatch }) => {
    const response = await api.delete(`/project/${id}`);
    if (response.status !== 200) {
      throw new Error("Server Error!");
    }
    dispatch(removeProject(id));
  }
);

export const addProjectThunk = createAsyncThunk(
  "projects/addProjectThunk",
  async (project: IProject, { dispatch }) => {
    const response = await api.post("/create/project", project);
    if (response.status !== 200) {
      throw new Error("Server Error!");
    }
    dispatch(addProject(project));
  }
);

export const editProjectThunk = createAsyncThunk(
  "projects/editProjectThunk",
  async (project: IProject, { dispatch }) => {
    const response = await api.patch("/edit/project", project);
    if (response.status !== 200) {
      throw new Error("Server Error!");
    }
    dispatch(editProject(project));
  }
);

const projectsSlice = createSlice({
  name: "projects",
  initialState,
  reducers: {
    removeProject: (state, action: PayloadAction<number>) => {
      state.all.filter((project) => project.projectId !== action.payload);
    },
    addProject: (state, action: PayloadAction<IProject>) => {
      state.all.push(action.payload);
    },
    editProject: (state, action: PayloadAction<IProject>) => {
      const project = action.payload;
      const index = state.all.findIndex(
        (item) => item.projectId === project.projectId
      );
      state.all[index] = project;
    }
  },
  extraReducers: (builder) => {
    builder.addCase(
      getProjectsThunk.fulfilled,
      (state, action: PayloadAction<ProjectsType>) => {
        const projects = action.payload;
        state.all = projects;
      }
    );
  }
});

export const { removeProject, addProject, editProject } = projectsSlice.actions;

export default projectsSlice.reducer;
