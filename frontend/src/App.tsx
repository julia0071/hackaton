/* eslint-disable @typescript-eslint/no-unused-vars */
import { ConfigProvider } from "antd";
import locale from "antd/locale/ru_RU";

import "@/assets/scss/index.scss";
import { theme } from "@/theme";
import { Routes, Route, Navigate } from "react-router";
import Auth from "@/components/common/Auth";
import Registration from "@/components/common/Registration";
import Profile from "@/components/common/Profile";
import PageLayout from "@/components/layout/PageLayout";
import PrivateAuthRoute from "@/hoc/PrivateAuthRoute";
import Projects from "@/pages/Projects";
import NotFound from "@/pages/NotFound";
import ProjectDetail from "@/pages/ProjectDetail";
import Tasks from "@/components/common/Tasks";
import TaskDetail from "@/pages/TaskDetail";
import CreateTask from "@/pages/CreateTask";
import CreateProject from "@/pages/CreateProject";
import EditProject from "@/pages/EditProject";

const App = () => {
  return (
    <ConfigProvider theme={theme} locale={locale}>
      <Routes>
        <Route path="/auth" element={<Auth />} />
        <Route path="/registration" element={<Registration />} />
        <Route element={<PrivateAuthRoute />}>
          <Route element={<PageLayout />}>
            <Route index element={<Navigate to="/dashboard" />} />
            <Route path="dashboard" element={<Profile />} />
            <Route path="projects">
              <Route index element={<Projects />} />
              <Route path="create" element={<CreateProject />} />
              <Route path=":id">
                <Route index element={<ProjectDetail />} />
                <Route path="edit" element={<EditProject />} />
              </Route>
            </Route>
            <Route path="tasks">
              <Route index element={<Tasks />} />
              <Route path="create" element={<CreateTask />} />
              <Route path=":id">
                <Route index element={<TaskDetail />} />
              </Route>
            </Route>
          </Route>
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </ConfigProvider>
  );
};

export default App;
