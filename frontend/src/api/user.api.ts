import { type IRegisterRequest } from "@/types/Register";
import { BASE_URL } from "./config";
import axios from "axios";

const userApi = {
  register: async (values: IRegisterRequest) => {
    const response = await axios.post(`/registration`, values, { baseURL: BASE_URL });
    if (response.status !== 200) {
      throw new Error("Server Error!");
    }
    return response.data;
  }
};

export default userApi;
