import { type IProject } from "@/types/Project";
import api from "./config";

const projectApi = {
  getProjectById: async (id: number) => {
    const response = await api.get<IProject>(`/project/${id}`);
    if (response.status !== 200) {
      throw new Error("Server Error!");
    }
    return response.data;
  }
};

export default projectApi;
