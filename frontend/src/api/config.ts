import axios from "axios";
import LocalStorage from "@/services/localStorage";
import { type IRefreshRequest, type IRefreshResponse } from "@/types/Auth";

// export const BASE_URL = `${process.env.BASE_HOST}${process.env.BASE_PORT}`;
export const BASE_URL = "http://localhost:8080";

const api = axios.create({
  baseURL: BASE_URL,
  withCredentials: false,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
    "Access-Control-Allow-Headers":
      "X-Requested-With, content-type, Authorization"
  }
});

api.interceptors.request.use(function (config) {
  const token = LocalStorage.get<string>("accessToken");
  config.headers.Authorization = token ? `Bearer ${token}` : "";
  return config;
});

api.interceptors.response.use(
  (config) => {
    return config;
  },
  async (error) => {
    const originalRequest = error.config;
    const refreshToken = LocalStorage.get<IRefreshRequest>("refreshToken");
    if (
      error.response.status === 401 &&
      error.config &&
      !error.config._isRetry
    ) {
      originalRequest._isRetry = true;
      try {
        const response = await axios.post<IRefreshResponse>(
          "/refresh-token",
          refreshToken
        );
        LocalStorage.set("accessToken", response.data.accessToken);
        LocalStorage.set("refreshToken", response.data.refreshToken);
        return await api.request(originalRequest);
      } catch (error) {
        console.log(error);
      }
    }
    throw error;
  }
);
export default api;
