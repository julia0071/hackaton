import { type UserRoleType } from "./Auth";

export interface IUser {
  userId: number;
  email: string;
  name: string;
  lastName: string;
  secondName: string;
}

export interface IUserInfo {
  userId: number;
  jobTitle: string;
  firstName: string;
  lastName: string;
  email: string;
  department: string;
  telephone: string;
  createdDate: string;
  location: {
    country: string;
    city: string;
  };
  role: UserRoleType;
  propertyDescription?: string;
}
