export interface IRegisterRequest {
  password: string;
  email: string;
  name: string;
  lastName: string;
  secondName: string | null;
}
