export type TaskCategoryType = "Task" | "Epic" | "Story" | "Bug";

export interface ITask {
  taskId: number;
  taskTitle: string;
  taskDescription: string;
  taskType: TaskCategoryType;
}

export type TasksType = ITask[];

export type TaskPriorityType = "High" | "Middle" | "Low";

export enum TaskPriorityEnum {
  High = "Высокий",
  Middle = "Средний",
  Low = "Низкий"
}

export type TasksViewType = "kanban" | "list";
