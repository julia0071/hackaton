export type StatusType = "start" | "loading" | "resolved" | "error";
