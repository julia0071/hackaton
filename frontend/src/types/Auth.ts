import { type StatusType } from "./Status";

export type UserRoleType = "ROLE_ADMIN" | "ROLE_USER" | "ROLE_PROJECT_MANAGER";

export interface IAuthResponse {
  accessToken: string;
  refreshToken: string;
  userId: number;
  role: UserRoleType;
}

export interface IAuthRequest {
  login: string;
  password: string;
}

export interface IRefreshRequest {
  refreshToken: string;
}

export interface IRefreshResponse {
  accessToken: string;
  refreshToken: string;
}

export interface AuthStateType {
  status: StatusType;
  user: number | null;
  role: UserRoleType | null;
}
