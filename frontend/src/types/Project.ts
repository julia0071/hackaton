import { type TasksType } from "./Task";

export interface IProject {
  projectId?: number;
  projectTitle: string;
  projectType: ProjectCategoryType;
  startDate: string;
  endDate: string;
  startDateFact?: string;
  endDateFact?: string;
  projectDescription?: string;
  projectStatusEnum: ProjectStatusType;
  projectOwnerId: number;
  comments?: unknown;
  tasks?: TasksType;
  creationDate?: string;
}

export type ProjectStatusType =
  | "Create"
  | "In_progress"
  | "Close"
  | "Cancelled";
export type ProjectCategoryType = "Investment" | "Custom" | "Support";

export enum ProjectTypeEnum {
  Investment = "Инвестиционный",
  Custom = "Заказной",
  Support = "Техническая поддержка"
}

export enum ProjectStatusEnum {
  Create = "Создан",
  In_progress = "В работе",
  Close = "Закрыт",
  Cancelled = "Отменён"
}

export enum TaskstStatusEnum {
  Create = "Создана",
  In_progress = "В работе",
  Close = "Закрыта",
  Cancelled = "Отменена"
}

export type ProjectsType = IProject[];
