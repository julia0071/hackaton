import { useAppSelector } from "@/hooks/useAppSelector";
import { Navigate, Outlet } from "react-router";

interface PropsType {
  redirectPath?: string;
}

const PrivateAuthRoute = ({
  redirectPath = "/auth"
}: PropsType): React.ReactNode => {
  const user = useAppSelector((state) => state.auth.user);

  if (!user) {
    return <Navigate to={redirectPath} replace />;
  }
  return <Outlet />;
};

export default PrivateAuthRoute;
