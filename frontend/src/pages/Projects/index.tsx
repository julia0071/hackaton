/* eslint-disable @typescript-eslint/no-unused-vars */
import ProjectsList from "@/components/common/Projects/ProjectList";
import Spinner from "@/components/ui/Spinner";
// import { type IProject } from "@/types/Project";
import { useAppDispatch } from "@/hooks/useAppDispatch";
import { useAppSelector } from "@/hooks/useAppSelector";
import { getProjectsThunk } from "@/store/slices/projectsSlice";
import { type StatusType } from "@/types/Status";
import { Alert } from "antd";
import { useEffect, useState } from "react";

const Projects = () => {
  const dispatch = useAppDispatch();
  const projects = useAppSelector((state) => state.projects.all);
  const [status, setStatus] = useState<StatusType>("resolved");
  // useEffect(() => {
  //   dispatch(getProjectsThunk())
  //     .unwrap()
  //     .then(() => {
  //       setStatus("resolved");
  //     })
  //     .catch(() => {
  //       setStatus("error");
  //     });
  // }, [dispatch]);
  // const status: StatusType = "resolved";
  // const projects: IProject[] = [
  //   {
  //     projectId: 1,
  //     projectTitle: "Title",
  //     projectType: "Investment",
  //     startDate: "10-10-2020",
  //     endDate: "10-11-2020",
  //     projectDescription: "Описание проекта",
  //     projectStatusEnum: "Create",
  //     projectOwnerId: 1,
  //     startDateFact: "10-10-2020",
  //     endDateFact: "10-10-2020",
  //     tasks: []
  //   },
  //   {
  //     projectId: 1,
  //     projectTitle: "Title",
  //     projectType: "Investment",
  //     startDate: "10-10-2020",
  //     endDate: "10-11-2020",
  //     projectDescription: "Описание проекта",
  //     projectStatusEnum: "Create",
  //     projectOwnerId: 1,
  //     startDateFact: "10-10-2020",
  //     endDateFact: "10-10-2020",
  //     tasks: []
  //   },
  //   {
  //     projectId: 1,
  //     projectTitle: "Title",
  //     projectType: "Investment",
  //     startDate: "10-10-2020",
  //     endDate: "10-11-2020",
  //     projectDescription: "Описание проекта",
  //     projectStatusEnum: "Create",
  //     projectOwnerId: 1,
  //     startDateFact: "10-10-2020",
  //     endDateFact: "10-10-2020",
  //     tasks: []
  //   },
  //   {
  //     projectId: 1,
  //     projectTitle: "Title",
  //     projectType: "Investment",
  //     startDate: "10-10-2020",
  //     endDate: "10-11-2020",
  //     projectDescription: "Описание проекта",
  //     projectStatusEnum: "Create",
  //     projectOwnerId: 1,
  //     startDateFact: "10-10-2020",
  //     endDateFact: "10-10-2020",
  //     tasks: []
  //   },
  //   {
  //     projectId: 1,
  //     projectTitle: "Title",
  //     projectType: "Investment",
  //     startDate: "10-10-2020",
  //     endDate: "10-11-2020",
  //     projectDescription: "Описание проекта",
  //     projectStatusEnum: "Create",
  //     projectOwnerId: 1,
  //     startDateFact: "10-10-2020",
  //     endDateFact: "10-10-2020",
  //     tasks: []
  //   }
  // ];
  return (
    <div>
      {status === "loading" && <Spinner />}
      {status === "resolved" && <ProjectsList projects={projects} />}
      {status === "error" && (
        <Alert type="error" showIcon message="Ошибка сервера" />
      )}
    </div>
  );
};

export default Projects;
