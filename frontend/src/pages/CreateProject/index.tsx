import FormLayout from "@/components/layout/ContentLayout";
import { Alert, Button, DatePicker, Form, Input, Select } from "antd";
import "./style.scss";
import dayjs from "dayjs";
import { type ProjectCategoryType } from "@/types/Project";
import { addProjectThunk } from "@/store/slices/projectsSlice";
import { useAppDispatch } from "@/hooks/useAppDispatch";
import { useState } from "react";
import { type StatusType } from "@/types/Status";
import Spinner from "@/components/ui/Spinner";
import { useAppSelector } from "@/hooks/useAppSelector";

interface FieldsType {
  projectOwnerId: number;
  projectDescription: string;
  projectTitle: string;
  projectType: ProjectCategoryType;
  date: string;
  startDate: string;
  endDate: string;
}

const CreateProject = () => {
  const [status, setStatus] = useState<StatusType>("start");
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const userId = useAppSelector((state) => state.auth.user);
  const onFinish = async (values: FieldsType) => {
    const { date, ...rest } = values;
    const formData = { ...rest };
    formData.projectOwnerId = userId as number;
    formData.startDate = dayjs(values.date[0]).format("YYYY-MM-DD");
    formData.endDate = dayjs(values.date[1]).format("YYYY-MM-DD");
    setStatus("loading");
    try {
      await dispatch(addProjectThunk(formData)).unwrap();
      setStatus("resolved");
    } catch (error) {
      setStatus("error");
    }
  };

  return (
    <>
      {status === "loading" && <Spinner />}
      {status === "start" && (
        <FormLayout>
          <Form
            name="project"
            form={form}
            style={{ maxWidth: 400 }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
            initialValues={{
              projectType: "Custom",
              projectDescription: null
            }}
          >
            <Form.Item<FieldsType>
              name="projectTitle"
              label="Название"
              rules={[{ required: true, message: "Введите название" }]}
            >
              <Input
                placeholder="Название"
                className="create-task__form-item"
              />
            </Form.Item>
            <Form.Item<FieldsType> name="projectType" label="Тип проекта">
              <Select
                placeholder="projectType"
                options={[
                  { value: "Custom", label: "Заказной" },
                  { value: "Investment", label: "Инвестиционный" },
                  { value: "Support", label: "Техническая поддержка" }
                ]}
                className="create-task__form-item"
              />
            </Form.Item>

            <Form.Item<FieldsType>
              label="Сроки проекта"
              name="date"
              rules={[{ required: true, message: "Введите cроки проекта" }]}
            >
              <DatePicker.RangePicker className="create-task__form-item" />
            </Form.Item>
            <Form.Item<FieldsType>
              label="Описание проекта"
              name="projectDescription"
            >
              <Input.TextArea
                className="create-task__form-item"
                placeholder="Описание"
                style={{ fontWeight: "inherit", height: 100, resize: "none" }}
              />
            </Form.Item>
            <Form.Item className="create-task__submit">
              <Button type="primary" htmlType="submit">
                Сохранить
              </Button>
            </Form.Item>
          </Form>
        </FormLayout>
      )}
      {status === "resolved" && (
        <Alert type="success" showIcon message="Проект успешно создан" />
      )}
      {status === "error" && (
        <Alert type="error" showIcon message="Ошибка сервера" />
      )}
    </>
  );
};

export default CreateProject;
