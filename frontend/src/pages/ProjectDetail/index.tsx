/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/prefer-nullish-coalescing */
import { Link, useNavigate, useParams } from "react-router-dom";
import "./style.scss";
import { useEffect, useState } from "react";
import { type StatusType } from "@/types/Status";
import projectApi from "@/api/project.api";
import {
  ProjectTypeEnum,
  type IProject,
  ProjectStatusEnum
} from "@/types/Project";
import {
  Alert,
  Button,
  Descriptions,
  Divider,
  type DescriptionsProps,
  Modal
} from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { deleteProjectThunk } from "@/store/slices/projectsSlice";
import { useAppDispatch } from "@/hooks/useAppDispatch";
import Spinner from "@/components/ui/Spinner";
import { useAppSelector } from "@/hooks/useAppSelector";

const ProjectDetail = () => {
  const { id } = useParams();
  const [status, setStatus] = useState<StatusType>("start");
  const projects = useAppSelector((state) => state.projects.all)
  const project = projects.find((project) => project.projectId === Number(id))
  // const [project, setProject] = useState<IProject>();

  // useEffect(() => {
  //   projectApi
  //     .getProjectById(Number(id))
  //     .then((project) => {
  //       setStatus("resolved");
  //       setProject(project);
  //     })
  //     .catch(() => {
  //       setStatus("error");
  //     });
  // }, []);

  const items: DescriptionsProps["items"] = [
    {
      key: "1",
      label: "Тип проекта",
      children: ProjectTypeEnum[project?.projectType]
    },
    {
      key: "7",
      label: "Статус проекта",
      children: ProjectStatusEnum[project?.projectStatusEnum]
    },
    {
      key: "2",
      label: "Создан",
      children: project?.creationDate
    },
    {
      key: "3",
      label: "Дата начала проекта",
      children: project?.startDate
    },
    {
      key: "4",
      label: "Дата окончания проекта",
      children: project?.endDate
    },
    {
      key: "5",
      label: "Фактическая дата начала",
      children: project?.startDateFact || "---"
    },
    {
      key: "6",
      label: "Фактическая дата окончания",
      children: project?.endDateFact || "---"
    }
  ];

  const [isModalOpen, setIsModalOpen] = useState(false);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = async () => {
    try {
      await dispatch(deleteProjectThunk(Number(id))).unwrap();
      setStatus("resolved");
    } catch (error) {
      setStatus("error");
    }
    setIsModalOpen(false);
    navigate("/projects");
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      {status === "loading" && <Spinner />}
      {status === "start" && project && (
        <>
          <Modal
            title="Удалить проект"
            open={isModalOpen}
            onOk={handleOk}
            onCancel={handleCancel}
          >
            Вы уверены, что хотите удалить проект?
          </Modal>
          <div className="project-detail">
            <h2>{project.projectTitle}</h2>
            <p>{project.projectDescription}</p>
            <Link to={`edit`}>
              <EditOutlined />
            </Link>
            <span
              onClick={showModal}
              style={{ cursor: "pointer", marginLeft: "1rem" }}
            >
              <DeleteOutlined />
            </span>
            <Divider />

            <Descriptions
              className="project-detail__info"
              layout="vertical"
              items={items}
            />
            <Link to="/tasks/create">
              <Button>Создать задачу</Button>
            </Link>
            {project.tasks?.length === 0 || project.tasks === null ? (
              <Alert
                className="project-detail__alert"
                type="info"
                showIcon
                message="По текущему проекту не создано ни одной задачи"
              />
            ) : (
              "Список задач"
            )}
          </div>
        </>
      )}
      {status === "error" && (
        <Alert type="error" showIcon message="Ошибка сервера" />
      )}
    </>
  );
};

export default ProjectDetail;
