import FormLayout from "@/components/layout/ContentLayout";
import { Button, DatePicker, Form, Input, Select } from "antd";
import "./style.scss";
const CreateTask = () => {
  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    console.log("Success:", values);
  };

  // const onFinishFailed = (errorInfo: any) => {
  //   console.log("Failed:", errorInfo);
  // };

  return (
    <FormLayout>
    <Form
      name="project"
      form={form}
      style={{ maxWidth: 400 }}
      onFinish={onFinish}
      autoComplete="off"
      layout="vertical"
      initialValues={{
        projectType: "Custom",
        projectDescription: null
      }}
    >
      <Form.Item<FieldsType>
        name="projectTitle"
        label="Название"
        rules={[{ required: true, message: "Введите название" }]}
      >
        <Input
          placeholder="Название"
          className="create-task__form-item"
        />
      </Form.Item>
      <Form.Item<FieldsType> name="projectType" label="Приоритет задачи">
        <Select
          placeholder="projectType"
          options={[
            { value: "Custom", label: "Высокий" },
            { value: "Investment", label: "Средний" },
            { value: "Support", label: "Низкий" }
          ]}
          className="create-task__form-item"
        />
      </Form.Item>

      <Form.Item<FieldsType>
        label="Сроки задачи"
        name="date"
        rules={[{ required: true, message: "Введите cроки задачи" }]}
      >
        <DatePicker.RangePicker className="create-task__form-item" />
      </Form.Item>
      <Form.Item<FieldsType>
        label="Описание задачи"
        name="projectDescription"
      >
        <Input.TextArea
          className="create-task__form-item"
          placeholder="Описание"
          style={{ fontWeight: "inherit", height: 100, resize: "none" }}
        />
      </Form.Item>
      <Form.Item className="create-task__submit">
        <Button type="primary" htmlType="submit">
          Сохранить
        </Button>
      </Form.Item>
    </Form>
  </FormLayout>
  );
};

export default CreateTask;
