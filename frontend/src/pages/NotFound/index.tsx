import { Result, Button } from "antd";
import "./style.scss";
import { useNavigate } from "react-router";

const NotFound = () => {
  const navigate = useNavigate();
  const toMainPage = () => {
    navigate("/");
  };
  return (
    <section className="not-found">
      <Result
        status="404"
        title="404"
        subTitle="Страницы не существует"
        extra={
          <Button onClick={toMainPage} type="default">
            На главную
          </Button>
        }
      />
    </section>
  );
};

export default NotFound;
